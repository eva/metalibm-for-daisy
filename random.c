/*

  Copyright 2007-2012 by

  Laboratoire de l'Informatique du Parallelisme,
  UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668,

  and by

  Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
  UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France.

  Contributor Ch. Lauter

  christoph.lauter@ens-lyon.org

  This software is a computer program whose purpose is to provide an
  environment for safe floating-point code development. It is
  particularly targeted to the automated implementation of
  mathematical floating-point libraries (libm). Amongst other features,
  it offers a certified infinity norm, an automatic polynomial
  implementer and a fast Remez algorithm.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use,
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info".

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability.

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or
  data to be ensured and,  more generally, to use and operate it in the
  same conditions as regards security.

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.

  This program is distributed WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*/

#include <gmp.h>
#include <mpfr.h>
#include <sollya.h>

int             random_gmp_state_initialized = 0;
gmp_randstate_t random_gmp_state;

static void random_gmp_state_initialize() {
  if (random_gmp_state_initialized) return;
  gmp_randinit_default(random_gmp_state);
  random_gmp_state_initialized = 1;
}

static void random_gmp_state_clear() {
  if (!random_gmp_state_initialized) return;
  gmp_randclear(random_gmp_state);
  random_gmp_state_initialized = 0;
}

static int random_inner(mpfr_t res, int prec) {
  mp_prec_t p;
  int pp;
  mpfr_t temp;
  
  p = (mp_prec_t) prec;
  pp = (int) p;
  if (pp != prec) return 0;

  random_gmp_state_initialize();
  
  mpfr_init2(temp, p);  
  mpfr_urandom(temp, random_gmp_state, GMP_RNDN);

  if (mpfr_get_prec(res) <= p) {
    mpfr_add_si(res, temp, 1, GMP_RNDD);
  } else {
    mpfr_add_si(temp, temp, 1, GMP_RNDD);
    mpfr_set(res, temp, GMP_RNDN);
  }
  mpfr_clear(temp);
  
  return 1;
}

/* Signature integer -> constant */
int random(mpfr_t *res, void **args) {
  return random_inner(*res, *((int *) (args[0])));
}

/* Cleanup */
int sollya_external_lib_close() {
  random_gmp_state_clear();
  return 0;
}
