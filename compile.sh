#!/bin/bash

sleep 1

LIB_F_NAME='lib_f'

if test "$1" = "--libfname"
then
    LIB_F_NAME="$2"
    shift
    shift
fi

unique=`basename "$2"`"-$$"
beforedir=`pwd`
if echo "$2" | grep -q '/'
then
    implementation="$2"
else
    implementation="$beforedir/"`basename "$2"`
fi
cd "$1"

SYSNAME=$(uname)
if test $SYSNAME = 'Darwin'
then
  echo "#define DARWIN 1" > "config.in.h"
else
  echo "#define DARWIN 0" > "config.in.h"
fi

echo "#include \"expansion.h\"" > "impl-$unique.c"
echo " " >> "impl-$unique.c"
cat "$implementation" | sed -e 's/void/static inline void/g;' | sed -e "s/static inline void $4(/void $4(/g;" >> "impl-$unique.c"
gcc -Winline -finline-limit=1200 -std=c99 -O2 -D$3 -DPOLYNOMIALNAME=$4 $6 -fPIC  -c "impl-$unique.c"
gcc -Winline -finline-limit=1200 -std=c99 -D$3 -DPOLYNOMIALNAME=$4 -DLIBFNAME=$LIB_F_NAME $6 -fPIC  -c expansion.c
gcc -std=c99 -shared -o "$5" "`basename "impl-$unique.c" .c`.o"  expansion.o -lgmp -lmpfr -lmpfi
rm "`basename "impl-$unique.c" .c`.o"
rm "impl-$unique.c"
rm expansion.o
cd $beforedir
sleep 1

