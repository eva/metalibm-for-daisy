/*

  Copyright 2007-2018 by

  Laboratoire de l'Informatique du Parallelisme,
  UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668,

  Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
  UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France

  and by

  Department of Computer Science & Engineering
  UAA College of Engineering
  University of Alaska Anchorage.

  Contributor Ch. Lauter

  christoph.lauter@ens-lyon.org

  This software is a computer program whose purpose is to provide an
  environment for safe floating-point code development. It is
  particularly targeted to the automated implementation of
  mathematical floating-point libraries (libm). Amongst other features,
  it offers a certified infinity norm, an automatic polynomial
  implementer and a fast Remez algorithm.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use,
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info".

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability.

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or
  data to be ensured and,  more generally, to use and operate it in the
  same conditions as regards security.

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.

  This program is distributed WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*/

#include <mpfr.h>
#include <sollya.h>
#include <Python.h>

/* Compile with 

   gcc -g -O0 -fPIC -I/home/lauter/sollya -I$(python3.5-config --cflags) -L$(python3.5-config --ldflags) -shared -o sollyapython.so sollyapython.c

   Use as

   externalproc(sollyapython, "./sollyapython.so", string -> void);
   sollyapython("print(17)");

*/

int sollyapython_initialized = 0;
wchar_t *sollyapython_program = NULL;

void sollyapython_initialize(void) {
  sollya_obj_t tmp;
  char *program;
  wchar_t *wprogram;
  
  if (sollyapython_initialized) return;
  
  tmp = sollya_lib_parse_string("(proc () { var res; if (isbound(__program_name)) then res = __program_name else res = \"sollya\"; return res; })()");
  if (sollya_lib_obj_is_string(tmp)) {
    if (sollya_lib_get_string(&program, tmp)) {
      wprogram = Py_DecodeLocale(program, NULL);
      if (wprogram != NULL) {
	Py_SetProgramName(wprogram);
	Py_Initialize();
	sollyapython_program = wprogram;
	sollyapython_initialized = 1;
      }
      sollya_lib_free(program);
    }
  }
  sollya_lib_clear_obj(tmp);
}

int sollyapython_finalize(void) {  
  if (!sollyapython_initialized) return 1;
  
  Py_Finalize();
  if (sollyapython_program != NULL) {
    PyMem_RawFree(sollyapython_program);
    sollyapython_program = NULL;
  }
  sollyapython_initialized = 0;
  
  return 1;
}

void sollyapython_inner(char *str) {
  sollyapython_initialize();
  PyRun_SimpleString(str);
}

/* Signature string -> void */
int sollyapython(void **args) {
  sollyapython_inner((char *) (args[0]));
  return 1;
}

int sollya_external_lib_close(void) {
  if (sollyapython_finalize()) return 0;
  return -1;
}

